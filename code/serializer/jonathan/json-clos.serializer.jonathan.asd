(asdf:defsystem #:json-clos.serializer.jonathan
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Artistic License 2.0"
  :depends-on ("json-clos" "json-clos.serializer.alist" "jonathan")
  :serial t
  :components ((:file "package")
               (:file "protocol")
               (:file "macros")
               (:file "jonathan")))
