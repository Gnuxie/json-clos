#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-clos.serializer.jonathan
  (:use #:cl #:json-clos.mop.serializable-class)
  (:export
   #:slot-serializer
   #:create-encoder
   #:serialize-slot
   #:clos<-json))
