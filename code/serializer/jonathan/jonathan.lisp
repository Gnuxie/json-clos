#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#
(in-package #:json-clos.serializer.jonathan)

(defmethod slot-serializer (slot effective-slotd class instance-var)
  (declare (ignore effective-slotd class))
  (let ((slot-name (c2mop:slot-definition-name slot)))
    `(when (slot-boundp ,instance-var ',slot-name)
       (jonathan:write-key-value
        ,(json-key slot)
        (slot-value object ',slot-name)))))

(defmethod create-encoder ((class json-serializable-class))
  `(defmethod jonathan:%to-json :around ((object ,(class-name class)))
     (jonathan:with-object
       ,@ (loop :for ((effective-slotd slot))
             :on (slot-precedence-list class)
             :collect (let ((start (gensym)))
                        `(tagbody ,start
                            (cond ((and (not (null additional-properties))
                                        (string< (caar additional-properties)
                                                 ,(json-key slot)))
                                   (let ((acons (pop additional-properties)))
                                     (jonathan:write-key-value (car acons)
                                                               (cdr acons)))
                                   (go ,start))
                                  (t (slot-serializer slot effective-slotd class 'object)))))))))

(defmethod serialize-slot ((slot json-serializable-slot) effective-slotd class instance)
  (within-object
    (when (c2mop:slot-boundp-using-class class instance effective-slotd)
      (jonathan:write-key-value
       (json-key slot)
       (c2mop:slot-value-using-class class instance effective-slotd)))))

(defmethod jonathan:%to-json ((object json-serializable))
  (let ((class (class-of object)))
    (with-object
      (loop :for ((effective-slotd slot))
         :on (slot-precedence-list class)
         :do (serialize-slot slot effective-slotd class object)))))

(defmethod jonathan:%to-json ((object json-serializable))
  (let* ((class (class-of object))
         (s-p-l (slot-precedence-list class))
         (additional-properties (additional-properties object)))
    (with-object
      (loop :until (and (null additional-properties)
                        (null s-p-l))
           :do (let ((next-additional-key (caar additional-properties)))
                    (destructuring-bind (slotd slot) (car s-p-l)
                      (cond ((and (not (null additional-properties))
                                   (string< next-additional-key
                                            (json-key slot)))
                             (let ((acons (pop additional-properties)))
                               (jonathan:write-key-value (car acons)
                                                         (cdr acons))))
                          (t (serialize-slot slot slotd class object)
                             (pop s-p-l)))))))))

;;; maybe we just use the slot-pl to get values from the alist representation
;;; then we can investiagte the performance hit from doing this
;;; i mean you'd have to do it with a remove-if to get the additional-properties
;;; anyways. The first port of call should be an alist serializer and deserializer
;;; to get this down.

#+ (or)
(defun key-normalizer (string)
  (declare (type string string))
  (declare (optimize (speed 3) (safety 1)))
  (loop :for c :across string
     :for i :from 0 :by 1 :do
       (setf (aref string i)
             (let ((char (char-upcase c)))
               (if (eql char #\_)
                   #\-
                   char))))
  string)

#+ (or)
(defmethod make-instance-from-json ((class json-serializable-class)
                                                    json)
  (apply #'make-instance class
         (jonathan:parse json :keyword-normalizer #'key-normalizer)))

(defgeneric clos<-json (class json)
  (:method ((class json-serializable-class) json)
    (json-clos.serializer.alist:instance<-alist
     class (jonathan:parse json :as :alist)))

  (:method ((class-name symbol) json)
    (clos<-json (find-class class-name) json)))
