#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-clos.serializer.alist
  (:use #:cl)
  (:local-nicknames (#:jcsc #:json-clos.mop.serializable-class))
  (:export
   #:alist<-instance
   #:instance<-alist
   #:create-encoder))

(in-package #:json-clos.serializer.alist)

;;; this is cursed.
(declaim (inline possible-class-name-p))
(defun possible-class-name-p (symbol)
  (and symbol
       (not (keywordp symbol))
       (symbolp symbol)))

(defgeneric alist<-instance (instance)
  (:method ((instance jcsc:json-serializable))
    (let* ((class (class-of instance))
           (slot-precedence-list (jcsc:slot-precedence-list class))
           (additional-properties (jcsc:additional-properties instance)))
      (loop :until (and (null slot-precedence-list)
                        (null additional-properties))

         :collect (let ((next-additional-key (caar additional-properties)))
                    (destructuring-bind (slotd slot) (car slot-precedence-list)
                      (if (and (not (null additional-properties))
                               (string< next-additional-key
                                        (jcsc:json-key slot)))
                          (pop additional-properties)
                          (prog1
                              (cons (jcsc:json-key slot)
                                    (c2mop:slot-value-using-class class instance slotd))
                            (pop slot-precedence-list)))))))))

(defmethod create-encoder ((class jcsc:json-serializable-class))
  `(defmethod alist<-instance ((instance ,(class-name class)))
     (let* ((additional-properties (jcsc:additional-properties instance))
            (output '()))
       ,@ (loop :for ((effective-slotd slot))
             :on (jcsc:slot-precedence-list class)
             :collect (let ((start (gensym)))
                        `(tagbody ,start
                            (cond ((and (not (null additional-properties))
                                        (string< (caar additional-properties)
                                                 ,(jcsc:json-key slot)))
                                   (push (pop additional-properties) output)
                                   (go ,start))
                                  (t (push
                                      (cons
                                       ,(jcsc:json-key slot)
                                       (slot-value instance
                                                   ',(c2mop:slot-definition-name slot)))
                                      output))))))
          (nreverse output))))

(defun hash-table-alist (hash-table)
  (declare (type hash-table hash-table))
  (mapcar
   (lambda (e)
     (typecase (cdr e)
       (hash-table (cons (car e) (hash-table-alist (cdr e))))
       (t e)))
   (alexandria:hash-table-alist hash-table)))

(defgeneric slot-initarg-value<-value (value json-type)
  (:method (value json-type)
    (cond ((or (null json-type) (keywordp json-type)) value)
          ((symbolp json-type)
           (instance<-alist json-type value))
          ((and (listp json-type)
                (listp value))
           (cond ((and (not (keywordp (jcsc:json-array-element-type json-type)))
                       (symbolp (jcsc:json-array-element-type json-type)))
                  (loop :for next-value :in value
                     :collect (slot-initarg-value<-value
                               next-value (jcsc:json-array-element-type json-type))))
                 (t value)))
          (t value)))
  (:method ((value hash-table) json-type)
    (slot-initarg-value<-value (hash-table-alist value) json-type))
  (:method  ((value number) (json-type (eql :string)))
    ;; there's a bug in libyaml where '2.0' = 2.0 for some bs reason
    ;; I just don't understand why you'd use YAML when it's so blatantly inconsistent
    (slot-initarg-value<-value (princ-to-string value) json-type)))

(defgeneric instance<-alist (class alist)
  (:method ((class jcsc:json-serializable-class) (alist list))
    ;; TODO make accessors for slotd pair instead of cadr and caar
    (let* ((class-name (class-name class))
           (s-p-l (jcsc:slot-precedence-list class))
           (initargs '())
           (additional-properties '()))
      (mapc (lambda (acons)
              (let ((slotd-pair (find (car acons) s-p-l
                                      :key (lambda (slotd-pair)
                                             (jcsc:json-key (jcsc:top-slot slotd-pair)))
                                      :test #'string-equal)))
                (if slotd-pair
                    (setf (getf initargs
                                (car (c2mop:slot-definition-initargs (jcsc:slotd slotd-pair))))
                          (let ((json-type  (jcsc:json-type (jcsc:top-slot slotd-pair))))
                            (slot-initarg-value<-value (cdr acons) json-type)))
                    (push acons additional-properties))))
            alist)
      (let ((instance (apply #'make-instance class-name initargs)))
        (setf (jcsc:additional-properties instance)
              additional-properties)
        (jcsc:apply-transformers instance class)
        instance)))

  (:method ((class jcsc:json-serializable-class) (alist hash-table))
    (instance<-alist class (alexandria:hash-table-alist alist)))

  (:method ((class-name symbol) alist)
    (instance<-alist (find-class class-name) alist)))

