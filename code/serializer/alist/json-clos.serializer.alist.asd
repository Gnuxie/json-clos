(asdf:defsystem #:json-clos.serializer.alist
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Artistic License 2.0"
  :depends-on ("json-clos")
  :serial t
  :components ((:file "alist")))
