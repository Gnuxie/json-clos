#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-clos
  (:use #:cl)
  (:import-from
   #:json-clos.mop.serializable-class
   
   #:json-serializable-class
   #:json-serializable-slot
   #:compute-slot-precedence-list
   #:slot-precedence-list
   #:json-key
   #:json-type
   #:json-serializable
   #:additional-properties
   #:top-slot
   #:slotd
   #:json-array-element-type
   #:pget

   ;; property-transforms
   #:additional-property-transformer
   #:additional-property-transformers   
   #:transform
   #:apply-transformers
   #:add-transformer
   )

  (:export
   #:json-serializable-class
   #:json-serializable-slot
   #:compute-slot-precedence-list
   #:slot-precedence-list
   #:json-key
   #:json-type
   #:json-serializable
   #:additional-properties
   #:top-slot
   #:slotd
   #:json-array-element-type
   #:pget

   ;; property-transforms
   #:additional-property-transformer
   #:additional-property-transformers
   #:transform
   #:apply-transformers
   #:add-transformer))
