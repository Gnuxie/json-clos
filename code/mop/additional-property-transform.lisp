#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-clos.mop.serializable-class)

(defclass additional-property-transformer () ())

(defgeneric transform (instance class transform property-name property)
  (:documentation "")
  (:method (instance class (transformer additional-property-transformer) property-name property)
    (declare (ignore instance class transformer property-name property))
    nil))

(defgeneric apply-transformers (instance class)
  (:documentation "Apply the transformers stored in the class to the
additional-properties in the instance.")
  (:method ((instance json-serializable) (class json-serializable-class))
    (loop :for property :in (additional-properties instance)
       :do (let ((first-transform-p nil)) ; we don't want two transforms to operate on the same
             ;; entry, this is undefined atm because whatever.
             (loop :for transformer :in (additional-property-transformers class)
                :for (transformation transformp)
                  := (multiple-value-list (transform instance class transformer (car property)
                                                     (cdr property)))
                :when transformp
                :do (setf (cdr property)
                          transformation)
                :and :do (setf first-transform-p t)
                :until first-transform-p)))))

(defgeneric add-transformer (transformer class)
  (:documentation "Adds the transform to the class"))
