#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>

This file is part of json-clos, it incorperates work from
cl-hash-util, which is available here https://github.com/orthecreedence/cl-hash-util
and contains the following copyright notice.

ALL work in json-clos is licensed under the Artistic License 2.0 unless otherwise stated.

Copyright (c) 2011 Lyon Bros LLC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
|#

(in-package #:json-clos.mop.serializable-class)

(defgeneric pget-access (obj key)
  (:method ((object json-serializable) (key string))
    (let ((additional-property
           (pget-access (additional-properties object)
                        key)))
      (if additional-property
          (values additional-property t)
          (let* ((class (class-of object))
                 (possible-slot-value
                  (loop :for (slotd slot) :in (slot-precedence-list class)
                     :when (string= key (json-key slot))
                     :do (return-from nil
                           (c2mop:slot-value-using-class class object slotd)))))
            (if possible-slot-value
                (values possible-slot-value t)
                (values nil nil))))))
  (:method ((hash-table hash-table) (key string))
    (gethash key hash-table))

  (:method ((list list) (key integer))
    (values (nth key list) t))

  (:method ((array array) (key integer))
    (values (aref array key) t))

  (:method ((array array) (key list))
    (values (apply #'aref array key) t))

  (:method ((list list) (key string))
    (let ((association (assoc key list :test #'string=)))
      (if association
          (values (cdr association) t)
          (values nil nil)))))

(defun pget (obj path &key fill-function)
  (declare (ignore fill-function))
  (let ((current-object obj)
        (last-value nil))
    (loop :for key :in path
       :for (value presentp) := (multiple-value-list (pget-access current-object key))
       :if presentp
       :do (setf last-value value)
       :and :do (setf current-object value)
       :else :do (return-from pget (values nil nil)))
    (values last-value t)))

;;; TODO finish this off.
#+ (or)
(defun (setf pget) (val obj path &key fill-function)
    (let ((current-object obj)
          (last-value nil)
          (path (nreverse (rest (reverse path)))))
    (loop :for key :in path
       :do (multiple-value-bind (value presentp) (pget-access current-object key)
             (if presentp
                (setf last-value value)
                (if fill-function
                    (setf ) (funcall fill-function)
                    (return-from pget (values nil nil))))))))
