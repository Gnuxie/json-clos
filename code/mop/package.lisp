#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-clos.mop.serializable-class
  (:use #:cl)
  (:export
   #:json-serializable-class
   #:json-serializable-slot
   #:compute-slot-precedence-list
   #:slot-precedence-list
   #:json-key
   #:json-type
   #:json-serializable
   #:additional-properties
   #:top-slot
   #:slotd
   #:json-array-element-type
   #:pget

   ;; property-transforms
   #:additional-property-transformer
   #:additional-property-transformers
   #:transform
   #:apply-transformers
   #:add-transformer
   ))
