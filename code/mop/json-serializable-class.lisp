#|
    Copyright (C) 2019-2020 Gnuxie <Gnuxie@protonmail.com>
    
    This file is a part of json-clos
    
    This file contains modified work from https://github.com/gschjetne/json-mop
    Copyright (C) 2015 Grim Schjetne
    See NOTICE.txt for details.
    
    The source code (as defined by the Artistic License) in this file is
    licensed under the Artistic License 2.0
    
    See the attached LICENSE
|#

(in-package #:json-clos.mop.serializable-class)

(defclass json-serializable-class (closer-mop:standard-class)
  ((%slot-precedence-list :reader slot-precedence-list
                          :type list
                          :documentation "
like the class-precedence-list except this list is sorted by json-key-name
of the slots, so that the json can easily be made canonical.
")
   (%additional-property-transformers :accessor additional-property-transformers
                                      :type list :initform (list)
                                      :documentation "A list of transformers
to apply to any additional-properties.")))

(defmethod c2mop:finalize-inheritance ((class json-serializable-class))
  (prog1 (call-next-method class)
    (setf (slot-value class '%slot-precedence-list)
          (compute-slot-precedence-list class))))

(defmethod compute-slot-precedence-list ((class json-serializable-class))
  "This is taken from #'SB-PCL::std-compute-slots in std-class"
  (let ((name-dslots-alist '()))
    (dolist (c (reverse (c2mop:class-precedence-list class)))
      (dolist (slot (c2mop:class-direct-slots c))
        (when (and (typep slot 'json-serializable-slot)
                   (slot-boundp slot 'json-key))
          (let* ((slot-name (c2mop:slot-definition-name slot))
                 (entry (assoc slot-name name-dslots-alist)))
            (if entry
                (push slot (cdr entry))
                (push (list slot-name slot) name-dslots-alist))))))
    (let ((effective-definitions (c2mop:class-slots class)))
      (sort
       (loop :for ((name slot &rest other-slots)) :on name-dslots-alist :collect
            (let ((effective-slot (find name effective-definitions
                                        :key #'c2mop:slot-definition-name)))
              (list effective-slot slot)))
       #'string<
       :key (lambda (slot-list)
              (destructuring-bind (effective-slot top-slot) slot-list
                (declare (ignore effective-slot))
                (json-key top-slot)))))))

(defmethod closer-mop:validate-superclass ((class json-serializable-class)
                                           (super closer-mop:standard-class))
  t)

(defmethod closer-mop:validate-superclass ((class standard-class)
                                           (super json-serializable-class))
  t)

(defclass json-serializable-slot (closer-mop:standard-direct-slot-definition)
  ((json-key :initarg :json-key
             :type string
             :reader json-key)
   (json-type :initarg :json-type
              :initform nil
              :type (or null list symbol)
              :reader json-type)))

(defmethod closer-mop:direct-slot-definition-class ((class json-serializable-class)
                                                    &rest initargs)
  (declare (ignore class initargs))
  (find-class 'json-serializable-slot))

(defclass json-serializable ()
  ((additional-properties :initarg :%additional-properties 
                          :accessor additional-properties
                          :initform nil
                          :type list
                          :documentation "A list of addtional properties
that are also associated with the instance.")))

(defmethod (setf additional-properties) :after (new-value (instance json-serializable))
  (declare (ignore new-value))
  (setf (slot-value instance 'additional-properties)
        (sort (slot-value instance 'additional-properties)
              #'string<
              :key #'car)))


(defmethod c2mop:class-direct-superclasses ((class json-serializable-class))
  (append (remove (find-class 'standard-object) (call-next-method))
          (list (find-class 'json-serializable)
                (find-class 'standard-object))))

(declaim (inline top-slot slot-definition slotd))
(defun top-slot (s-p-l-entry)
  (cadr s-p-l-entry))

(defun slotd (s-p-l-entry)
  (car s-p-l-entry))

(declaim (inline json-array-element-type))
(defun json-array-element-type (json-array-type-specifier)
  (cadr json-array-type-specifier))

