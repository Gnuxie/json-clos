(asdf:defsystem #:json-clos.test
  :depends-on ("parachute" "json-clos" "json-clos.serializer.jonathan")
  :serial t
  :components ((:file "package")
               (:module "serializer" :components
                        ((:module "jonathan"
                                  :components ((:file "package")
                                               (:file "jonathan")
                                               (:file "json-mop-example")))))))
