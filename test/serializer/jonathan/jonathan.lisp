#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-clos.test.serializer.jonathan)

(defclass book ()
  ((author :initarg :author :json-key "author")
   (date :initarg :date :json-key "date")
   (isbn :initarg :isbn :json-key "isbn")
   (publisher :initarg :publisher :json-key "publisher")
   (|underworld-publisher-that's-secret|))
  (:metaclass jcsc:json-serializable-class))

(c2mop:finalize-inheritance (find-class 'book))

(p:define-test encode/decode
  :parent #:json-clos.test.serializer.jonathan

  (let* ((alist-form
          '(("author" . "phoe") ("date" . "2020-04-25") ("isbn" . "clcs-1312")
            ("publisher" . "apress") ("irc" . "#clcs")))
         (string-form (jonathan:to-json (sort (copy-alist alist-form) #'string< :key #'car)
                                        :from :alist))
         (instance (jonathan-clos:clos<-json 'book string-form)))
    (p:is string= string-form
          (jonathan:to-json instance))))
