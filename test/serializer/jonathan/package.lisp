#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-clos.test.serializer.jonathan
  (:use #:cl)
  (:local-nicknames (#:p #:parachute)
                    (#:jcsc #:json-clos.mop.serializable-class)
                    (#:jonathan-clos #:json-clos.serializer.jonathan))
  (:export
   #:json-clos.test.serializer.jonathan))

(in-package #:json-clos.test.serializer.jonathan)

(p:define-test json-clos.test.serializer.jonathan
  :parent (#:json-clos.test #:json-clos.test))
