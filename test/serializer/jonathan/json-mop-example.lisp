#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-clos.test.serializer.jonathan)

(defclass book ()
  ((title :initarg :title
          :json-key "title")
   (published-year :initarg :year
                   :json-key "year_published")
   (fiction :initarg :fiction
            :json-key "is_fiction"))
  (:metaclass jcsc:json-serializable-class))

(defclass author ()
  ((name :initarg :name
         :json-type :string
         :json-key "name")
   (birth-year :initarg :year
               :json-type :number
               :json-key "year_birth")
   (bibliography :initarg :bibliography
                 :json-type (:array book) ; this is different
                 :json-key "bibliography"))
  (:metaclass jcsc:json-serializable-class))

(p:define-test json-mop-example
  :parent #:json-clos.test.serializer.jonathan

  (let* ((author
          (make-instance 'author
                         :name "Mark Twain"
                         :year 1835
                         :bibliography
                         (list
                          (make-instance 'book
                                         :title "The Gilded Age: A Tale of Today"
                                         :year 1873
                                         :fiction t)
                          (make-instance 'book
                                         :title "Life on the Mississippi"
                                         :year 1883
                                         :fiction nil)
                          (make-instance 'book
                                         :title "Adventures of Huckleberry Finn"
                                         :year 1884
                                         :fiction t))))
         (author-serial (jonathan:to-json author))
         (new-author (json-clos.serializer.jonathan:clos<-json 'author author-serial)))
    (p:true (typep (car (slot-value new-author 'bibliography))
                   'book))
    (p:is #'= 1884
          (json-clos:pget author '("bibliography" 2 "year_published")))))
