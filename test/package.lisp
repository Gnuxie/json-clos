#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-clos.test
  (:use #:cl)
  (:local-nicknames (#:p #:parachute))
  (:export
   #:json-clos.test
   #:run
   #:ci-run))

(in-package #:json-clos.test)

(p:define-test json-clos.test)

(defun run (&key (report 'p:plain))
  (p:test 'json-clos.test :report report))

(defun ci-run ()
  (let ((test-result (run)))
    (when (not (null (p:results-with-status :failed test-result)))
      (uiop:quit -1))))
