# json-clos

json-clos is another library for bridging clos and json objects, essentially [json-mop](https://github.com/gschjetne/json-mop) 2 but without stealing the name.

The main features over json-mop are supporting inheritance, independent serializers and a protocol for storing extra properties. We also sort the slots in the slot-precedence-list so that it is possible for serializers to emit 'Canonical JSON'.

## type specifiers

Serializers may ignore type specifiers completely.
Type specifiers in json-clos are focused on the type the json representation is
and not the decoded representation. Therefor initially we use the set of
type specifiers from [json-schema](https://tools.ietf.org/html/draft-handrews-json-schema-validation-02#section-6.1) type validation. These are represented as keywords `:null`, `:boolean`,
`:object`, `:number` `:array` and `:string`. We will also allow the symbol that names a json-serializable-class to be used as a json-type specifier. A compound type specifier like `cl:array` can be used with `:array`. Serializers may divide the types up further e.g. `:number` but cannot expect support from other serializers. If `nil` is provided as a json-type then we expect the json to be of any type. Finally `cl:or` may be used to compound multiple type specifiers together.

## properties

We intend to provide a protocol like cl-hash-util to access properties from an instance. This may eventually allow for the internal representation of `%additional-properties` to be irrelevant to end users.


## examples

check the test or examples directory.
