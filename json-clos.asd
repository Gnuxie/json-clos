(asdf:defsystem #:json-clos
  :author "Gnuxie <Gnuxie@protonmail.com"
  :license "Artistic License 2.0"
  :version "0.0.1"
  :description "Just like json-mop but improved and serializer independant"
  :depends-on ("closer-mop")
  :serial t
  :components ((:module "code" :components
                        ((:module "mop" :components
                                  ((:file "package")
                                   (:file "json-serializable-class")
                                   (:file "property-access-protocol")
                                   (:file "additional-property-transform")))
                         (:file "package")))))
